package com.example.mavengitflow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;

@Controller
@EnableAutoConfiguration
@SpringBootApplication
public class MavenGitflowApplication {

	public static void main(String[] args) {
		SpringApplication.run(MavenGitflowApplication.class, args);
	}

    @RestController
    @EnableAutoConfiguration
    public class Helloworld {
        @RequestMapping("/")
        String home() {
            return "Hello feature 0.0.4";
        }
}
}
